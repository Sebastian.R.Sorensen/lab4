package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int cols;
    private CellState[][] grid;



    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.rows = rows;
        this.cols = columns;
        grid = new CellState [rows][columns];
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < columns; y++) {
                grid [x][y] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return this.rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (row >= 0 && row < numRows() && column >= 0 && column < numColumns()) {
            grid[row][column] = element;  
        } else {
            throw new IndexOutOfBoundsException("Index is out of bounds");
        }  

        // TODO Auto-generated method stub
        
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        if (row >= 0 && row < numRows() && column >= 0 && column < numColumns()) {
            return grid[row][column];
        } else {
            throw new IndexOutOfBoundsException("Index is out of bounds");
        }       

    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        IGrid copygrid = new CellGrid(this.rows, this.cols, CellState.DEAD);
        for (int x = 0; x < rows ; x++) {
            for (int y = 0; y < cols; y++) {
                copygrid.set(x, y, get(x, y));
            }
        }
        return copygrid;
    }

    
    
}
