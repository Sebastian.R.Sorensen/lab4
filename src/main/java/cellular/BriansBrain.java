package cellular;

public class BriansBrain extends GameOfLife {
    public BriansBrain(int rows, int columns) {
        super(rows, columns);
    }
    
    @Override
    public CellState getNextCell(int row, int col) {
        CellState state = getCellState(row, col);
		int aliveNeighbor = super.countNeighbors(row, col, CellState.ALIVE);

        if (state.equals(CellState.ALIVE)) return CellState.DYING;
        else if (state.equals(CellState.DYING)) return CellState.DEAD;
        else if (state.equals(CellState.DEAD) && aliveNeighbor == 2) return CellState.ALIVE;
        else return CellState.DEAD;

    }
}
